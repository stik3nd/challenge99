package com.direito.challenge99;

import android.support.test.espresso.action.ViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import com.direito.challenge99.pickupaddress.PickupAddressActivity;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class PickupAddressScreenTest {
    @Rule
    public ActivityTestRule<PickupAddressActivity> pickupAddressActivityActivityTestRule =
            new ActivityTestRule<PickupAddressActivity>(PickupAddressActivity.class);

    @Test
    public void searchAddress() throws Exception {
        String street = "Av. Paulista";
        String number = "100";

        onView(withId(R.id.streetEditText)).perform(typeText(street), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.numberEditText)).perform(typeText(number), ViewActions.closeSoftKeyboard());

        onView(withId(R.id.action_done)).perform(click());
    }
}
