package com.direito.challenge99.ordercab;

import android.content.Context;
import com.direito.challenge99.data.OrderCabRepository;
import com.direito.challenge99.data.api.callback.DriverCallback;
import com.direito.challenge99.data.api.callback.RideCallback;
import com.direito.challenge99.data.api.model.Driver;
import com.direito.challenge99.data.api.model.Ride;
import com.google.android.gms.maps.model.LatLng;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

public class OrderCabPresenterTest {

    private static List<Driver> DRIVERS;
    private static Ride ride;
    public static final String rideMessage = "Driver is on the way!";

    @Mock
    private OrderCabRepository orderCabRepository;
    @Mock
    private OrderCabContract.View orderCabView;
    @Mock
    private Context context;

    @Captor
    private ArgumentCaptor<DriverCallback> driverCallbackArgumentCaptor;
    @Captor
    private ArgumentCaptor<RequestRideCallback> requestRideCallbackArgumentCaptor;
    @Captor
    private ArgumentCaptor<RideCallback> rideCallbackArgumentCaptor;

    private OrderCabPresenter orderCabPresenter;

    @Before
    public void setupOrderCabPresenter() {
        MockitoAnnotations.initMocks(this);

        orderCabPresenter = new OrderCabPresenter(orderCabView, orderCabRepository);

        DRIVERS = Arrays.asList(
                new Driver(0.0, 0.0, 1, true),
                new Driver(0.0, 0.0, 2, true),
                new Driver(0.0, 0.0, 3, true)
        );

        ride = new Ride(1, rideMessage);
    }

    @Test
    public void loadDriversFromRepositoryAndShowInView() {
        String southWestLatLng = "-23.612474,-46.702746";
        String northEastLatLng = "-23.589548,-46.673392";

        orderCabPresenter.loadDrivers(southWestLatLng, northEastLatLng);

        verify(orderCabRepository).getNearbyDrivers(eq(southWestLatLng), eq(northEastLatLng), driverCallbackArgumentCaptor.capture());
        driverCallbackArgumentCaptor.getValue().onDriversLoaded(DRIVERS);

        ArgumentCaptor<List> displayDriversArgumentCaptor = ArgumentCaptor.forClass(List.class);
        verify(orderCabView).displayDrivers(displayDriversArgumentCaptor.capture());
        assertTrue(displayDriversArgumentCaptor.getValue().size() == 3);
    }

    @Test
    public void loadUserLocationAndShowInView() {
        orderCabPresenter.loadLocation(context);
        ArgumentCaptor<LatLng> latLngArgumentCaptor = ArgumentCaptor.forClass(LatLng.class);
        verify(orderCabView).updatePosition(latLngArgumentCaptor.capture());
    }

    @Test
    public void requestRide() {
        String rideUrl = "http://ec2-54-88-12-34.compute-1.amazonaws.com:8080/v1/ride/42";

        orderCabPresenter.requestRide();
        verify(orderCabView).showLoading();
        verify(orderCabRepository).requestRide(requestRideCallbackArgumentCaptor.capture());
        requestRideCallbackArgumentCaptor.getValue().onRideRequested(rideUrl);

        verify(orderCabRepository).getRide(eq(rideUrl), rideCallbackArgumentCaptor.capture());
        rideCallbackArgumentCaptor.getValue().onRideLoaded(ride);
        verify(orderCabView).showContent();
        verify(orderCabView).showPersistentMessage(eq(ride.getMessage()));
    }


}
