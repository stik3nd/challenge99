package com.direito.challenge99.ordercab;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.direito.challenge99.data.OrderCabRepository;
import com.direito.challenge99.data.api.callback.DriverCallback;
import com.direito.challenge99.data.api.callback.RideCallback;
import com.direito.challenge99.data.api.model.Driver;
import com.direito.challenge99.data.api.model.Ride;
import com.direito.challenge99.pickupaddress.PickupAddressActivity;
import com.direito.challenge99.util.LocationUtils;
import com.direito.challenge99.util.Transformers;
import com.google.android.gms.maps.model.LatLng;
import rx.Observer;

import java.util.List;

public class OrderCabPresenter implements OrderCabContract.Presenter {
    private final OrderCabContract.View orderCabView;
    private final OrderCabRepository orderCabRepository;

    public OrderCabPresenter(OrderCabContract.View orderCabView, OrderCabRepository orderCabRepository) {
        this.orderCabView = orderCabView;
        this.orderCabRepository = orderCabRepository;
    }

    @Override
    public void result(int requestCode, int resultCode, Intent data) {
        if (requestCode == PickupAddressActivity.REQUEST_CODE_FIND_ADDRESS && resultCode == Activity.RESULT_OK) {
            LatLng foundAddressLatLng = data.getExtras().getParcelable(PickupAddressActivity.ARG_LAT_LNG);
            if (foundAddressLatLng != null)
                orderCabView.updatePosition(foundAddressLatLng);
        }
    }

    @Override
    public void loadDrivers(final String southWestLatLng, final String northEastLatLng) {
        orderCabRepository.getNearbyDrivers(
                southWestLatLng,
                northEastLatLng,
                new DriverCallback() {
                    @Override
                    public void onDriversLoaded(List<Driver> drivers) {
                        orderCabView.displayDrivers(drivers);
                    }

                    @Override
                    public void onLoadError(String errorResponse) {
                        orderCabView.showMessage(errorResponse);
                    }
                });
    }

    @Override
    public void loadLocation(final Context context) {
        orderCabView.updatePosition(
                LocationUtils.getMyLastPosition(context)
        );
    }

    @Override
    public void loadAddress(final Context context, final LatLng lastPosition) {
        if (lastPosition != null) {
            orderCabRepository.getAddress(context, lastPosition.latitude, lastPosition.longitude)
                    .compose(Transformers.applySchedulers())
                    .subscribe(orderCabView::updateAddress);
        }
    }

    @Override
    public void requestRide() {
        orderCabView.showLoading();
        orderCabRepository.requestRide()
                .compose(Transformers.applySchedulers())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        onRequestError("Error: " + e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(String rideUrl) {
                        getRide(rideUrl);
                    }
                });

//        orderCabRepository.requestRide(new RequestRideCallback() {
//            @Override
//            public void onRideRequested(String rideUrl) {
//                getRide(rideUrl);
//            }
//
//            @Override
//            public void onLoadError(String errorResponse) {
//                orderCabView.showError();
//                orderCabView.showMessage(errorResponse);
//            }
//        });
    }

    private void onRequestError(String message) {
        orderCabView.showError();
        orderCabView.showMessage(message);
    }

    @Override
    public void getRide(final String rideUrl) {
        orderCabRepository.getRide(rideUrl, new RideCallback() {
                    @Override
                    public void onRideLoaded(Ride ride) {
                        orderCabView.showContent();
                        orderCabView.showPersistentMessage(ride.getMessage());
                    }

                    @Override
                    public void onLoadError(String errorResponse) {
                        orderCabView.showError();
                        orderCabView.showMessage(errorResponse);
                    }
                });
    }

    @Override
    public void onStart() {
    }

    @Override
    public void onStop() {

    }
}
