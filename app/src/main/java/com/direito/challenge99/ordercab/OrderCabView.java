package com.direito.challenge99.ordercab;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.direito.challenge99.R;

public class OrderCabView extends LinearLayout {

    @BindView(R.id.addressTextView)
    TextView addressTextView;

    public interface Listener {
        void onContainerClicked();
    }

    private Listener listener;

    public OrderCabView(Context context) {
        super(context);
        init();
    }

    public OrderCabView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initWithStyle(context, attrs);
    }

    public OrderCabView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initWithStyle(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public OrderCabView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initWithStyle(context, attrs);
    }


    private void initWithStyle(final Context context, final AttributeSet attrs) {
//        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.OrderCabView);
//        typedArray.recycle();

        init();
    }

    private void init() {
        View rootView = inflate(getContext(), R.layout.view_order_cab, this);
        ButterKnife.bind(this, rootView);
    }

    public void setAddress(String address) {
        if (address != null && addressTextView != null)
            addressTextView.setText(address);
    }

    @OnClick(R.id.orderCabViewContainer)
    public void onContainerClicked() {
        if (listener != null) {
            listener.onContainerClicked();
        }
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }
}

