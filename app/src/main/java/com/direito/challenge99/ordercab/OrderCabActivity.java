package com.direito.challenge99.ordercab;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import com.direito.challenge99.R;
import com.direito.challenge99.common.BaseActivity;
import com.direito.challenge99.common.BasePresenter;
import com.direito.challenge99.internal.di.components.ApplicationComponent;
import com.direito.challenge99.persistence.AppStorageManagerImpl;
import com.direito.challenge99.util.Navigator;

public class OrderCabActivity extends BaseActivity
        implements
            NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    private AppStorageManagerImpl sharedPreferences;

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_order_cab;
    }

    @Override
    public void setUpComponent(ApplicationComponent appComponent) {
        sharedPreferences = appComponent.getSharedPreferences();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        Navigator.callOrderCabFragment(getSupportFragmentManager(), R.id.orderCabContent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupNavigationViewHeader();
    }

    private void setupNavigationViewHeader() {
        TextView registerTextView = (TextView) navigationView.getHeaderView(0).findViewById(R.id.registerTextView);
        ViewGroup userViewGroup = (ViewGroup) navigationView.getHeaderView(0).findViewById(R.id.userViewGroup);
        TextView nameTextView = (TextView) navigationView.getHeaderView(0).findViewById(R.id.nameTextView);

        navigationView.getHeaderView(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigator.callUserActivity(OrderCabActivity.this);
            }
        });

        boolean hasUser = sharedPreferences.containsUser();
        if (hasUser) {
            registerTextView.setVisibility(View.GONE);
            nameTextView.setText(sharedPreferences.getUser().getName());
            userViewGroup.setVisibility(View.VISIBLE);
        } else {
            userViewGroup.setVisibility(View.GONE);
            registerTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
