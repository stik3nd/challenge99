package com.direito.challenge99.ordercab;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import butterknife.BindView;
import com.direito.challenge99.R;
import com.direito.challenge99.common.BaseFragment;
import com.direito.challenge99.common.BasePresenter;
import com.direito.challenge99.data.api.model.Driver;
import com.direito.challenge99.internal.di.components.ApplicationComponent;
import com.direito.challenge99.internal.di.components.DaggerOrderCabComponent;
import com.direito.challenge99.internal.di.modules.OrderCabModule;
import com.direito.challenge99.util.*;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;
import rx.Observable;
import rx.Subscription;

import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class OrderCabFragment extends BaseFragment
        implements
            OnMapReadyCallback,
            GoogleMap.OnCameraChangeListener,
            OrderCabContract.View,
            View.OnClickListener {

    public static final String TAG = "OrderCabFragment";

    @BindView(R.id.requestRideFAB)
    FloatingActionButton requestRideFAB;
    @BindView(R.id.orderCabView)
    OrderCabView orderCabView;
    @BindView(R.id.mapView)
    MapView mapView;
    private GoogleMap map;
    private boolean mPermissionDenied = false;
    private boolean myLocationClicked = false;
    private LatLng lastPosition;
    private BitmapDescriptor icon;
    private Subscription pollDriversPositionSubscription;

    @Inject
    OrderCabPresenter orderCabPresenter;

    @Override
    protected void setUpComponent(ApplicationComponent component) {
        DaggerOrderCabComponent.builder()
                .applicationComponent(component)
                .orderCabModule(new OrderCabModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected BasePresenter getPresenter() {
        return orderCabPresenter;
    }

    @Override
    public String tag() {
        return TAG;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_order_cab;
    }

    public OrderCabFragment() {
    }

    public static OrderCabFragment newInstance() {
        OrderCabFragment fragment = new OrderCabFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView.onCreate(savedInstanceState);
//        mapView.onCreate(Bundle.EMPTY);

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            Log.e(TAG, "Erro na inicializacao do mapa " + e.getLocalizedMessage());
        }
        mapView.getMapAsync(this);
        requestRideFAB.setOnClickListener(this);
        orderCabView.setListener(() -> Navigator.callPickupAddressActivity(OrderCabFragment.this));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        orderCabPresenter.result(requestCode, resultCode, data);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mapView != null) {
            mapView.onResume();
            createDriversPolling();
        }

        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showRationaleMessage();
            mPermissionDenied = false;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mapView != null)
            mapView.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mapView != null)
            mapView.onPause();
        pollDriversPositionSubscription.unsubscribe();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mapView != null)
            mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null)
            mapView.onPause();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        icon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_local_taxi_black_24dp);

        map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                myLocationClicked = true;
                orderCabPresenter.loadLocation(CONTEXT);
                return false;
            }
        });
        enableMyLocation();
        map.getUiSettings().setAllGesturesEnabled(true);
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().setMapToolbarEnabled(true);
        map.setOnCameraChangeListener(this);

        createDriversPolling();
    }

    @Override
    public void updatePosition(final LatLng lastPosition) {
        if (lastPosition != null) {
            setLastPosition(lastPosition);
            LocationUtils.moveCamera(map, this.lastPosition);

            orderCabPresenter.loadAddress(CONTEXT, lastPosition);
        }
    }

    @Override
    public void updateAddress(String address) {
        if (orderCabView != null && !StringUtils.isNullOrEmpty(address))
            orderCabView.setAddress(address);
    }

    private void createDriversPolling() {
        if (pollDriversPositionSubscription == null || pollDriversPositionSubscription.isUnsubscribed()) {
            pollDriversPositionSubscription =
                    Observable.interval(5, TimeUnit.SECONDS)
                            .compose(Transformers.applySchedulers())
                            .map((tick) -> pollDriversPosition())
                            .retry()
                            .subscribe();
        }
    }

    private Observable pollDriversPosition() {
        LatLngBounds bounds = map.getProjection()
                .getVisibleRegion().latLngBounds;

        if (bounds.southwest.latitude != 0) {
            orderCabPresenter.loadDrivers(
                    LocationUtils.getFormattedLatLng(bounds.southwest),
                    LocationUtils.getFormattedLatLng(bounds.northeast));
        }

        return Observable.empty();
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        float minZoom = 11.0f;
        float maxZoom = 19.0f;

        if (myLocationClicked) {
            map.animateCamera(CameraUpdateFactory.zoomTo(LocationUtils.DEFAULT_ZOOM_LEVEL));
            myLocationClicked = false;
        } else {

            if (cameraPosition.zoom < minZoom)
                map.animateCamera(CameraUpdateFactory.zoomTo(minZoom));

            if (cameraPosition.zoom > maxZoom)
                map.animateCamera(CameraUpdateFactory.zoomTo(maxZoom));
        }

    }

    private void setLastPosition(LatLng lastPosition) {
        this.lastPosition = lastPosition;
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(CONTEXT, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            PermissionUtils.requestPermission(this, PermissionUtils.REQUEST_LOCATION,
                    android.Manifest.permission.ACCESS_FINE_LOCATION, true);

        } else if (map != null) {
            map.setMyLocationEnabled(true);
            orderCabPresenter.loadLocation(CONTEXT);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode != PermissionUtils.REQUEST_LOCATION) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            enableMyLocation();
        } else {
            mPermissionDenied = true;
        }
    }

    private void showRationaleMessage() {
        AlertUtils.showMessage(getActivity().getWindow().getDecorView().findViewById(android.R.id.content), getResources().getString(R.string.permission_required_toast));
    }

    @Override
    public void displayDrivers(List<Driver> drivers) {
        if (map != null && drivers != null && drivers.size() > 0) {
            map.clear();
            //Add user marker
            map.addMarker(new MarkerOptions()
                    .position(lastPosition));

            for (int i = 0; i < drivers.size(); i++) {
                map.addMarker(
                        new MarkerOptions()
                            .icon(icon)
                            .position(new LatLng(
                                    drivers.get(i).getLatitude(),
                                    drivers.get(i).getLongitude())
                            )
                );
            }
        }
    }

    @Override
    public void showMessage(final String message) {
        AlertUtils.showMessage(
                getActivity().getWindow().getDecorView().findViewById(android.R.id.content),
                message);
    }

    @Override
    public void showPersistentMessage(final String message) {
        AlertUtils.showMessageDialog(CONTEXT, message);
    }

    @Override
    public void showLoading() {
        showProgressLoading();
    }

    @Override
    public void showError() {
        hideProgressLoading();
    }

    @Override
    public void showContent() {
        hideProgressLoading();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.requestRideFAB:
                orderCabPresenter.requestRide();
                break;
        }
    }

}
