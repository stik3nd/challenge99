package com.direito.challenge99.ordercab;

import android.content.Context;
import android.content.Intent;
import com.direito.challenge99.common.BasePresenter;
import com.direito.challenge99.common.BaseView;
import com.direito.challenge99.data.api.model.Driver;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public interface OrderCabContract {

    interface View extends BaseView {

        void displayDrivers(List<Driver> drivers);

        void showMessage(String message);

        void showPersistentMessage(String message);

        void updatePosition(LatLng lastPosition);

        void updateAddress(String address);

    }

    interface Presenter extends BasePresenter {

        void result(int requestCode, int resultCode, Intent data);

        void loadDrivers(final String southWestLatLng, final String northEastLatLng);

        void loadLocation(final Context context);

        void loadAddress(final Context context, LatLng lastPosition);

        void requestRide();

        void getRide(final String rideUrl);

    }
}
