package com.direito.challenge99.pickupaddress;

import android.content.Context;
import com.direito.challenge99.common.BasePresenter;
import com.direito.challenge99.common.BaseView;
import com.google.android.gms.maps.model.LatLng;

public interface PickupAddressContract {

    interface View extends BaseView {

        void showMessage(String message);

        void updateLocation(LatLng latLng);
    }

    interface Presenter extends BasePresenter {

        void searchAddress(final Context context, final String address);

    }
}
