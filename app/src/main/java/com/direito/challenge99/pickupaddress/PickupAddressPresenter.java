package com.direito.challenge99.pickupaddress;

import android.content.Context;
import com.direito.challenge99.R;
import com.direito.challenge99.data.OrderCabRepository;
import com.direito.challenge99.util.Transformers;
import com.google.android.gms.maps.model.LatLng;
import rx.Observer;

public class PickupAddressPresenter implements PickupAddressContract.Presenter {

    private final PickupAddressContract.View pickupAddressView;
    private final OrderCabRepository orderCabRepository;

    public PickupAddressPresenter(PickupAddressContract.View pickupAddressView, OrderCabRepository orderCabRepository) {
        this.pickupAddressView = pickupAddressView;
        this.orderCabRepository = orderCabRepository;
    }

    @Override
    public void searchAddress(final Context context, final String address) {
        pickupAddressView.showLoading();
        orderCabRepository.getLatLngFromAddress(context, address)
                .compose(Transformers.applySchedulers())
                .subscribe(new Observer<LatLng>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        pickupAddressView.showError();
                        pickupAddressView.showMessage("Error: " + e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(LatLng latLng) {
                        if (latLng == null) {
                            pickupAddressView.showError();
                            pickupAddressView.showMessage(context.getString(R.string.not_found));
                            return;
                        }

                        pickupAddressView.showContent();
                        pickupAddressView.updateLocation(latLng);
                    }
                });
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }
}
