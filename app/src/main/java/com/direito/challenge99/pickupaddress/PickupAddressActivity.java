package com.direito.challenge99.pickupaddress;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import butterknife.BindView;
import com.direito.challenge99.R;
import com.direito.challenge99.common.BaseActivity;
import com.direito.challenge99.common.BasePresenter;
import com.direito.challenge99.internal.di.components.ApplicationComponent;
import com.direito.challenge99.util.Navigator;

public class PickupAddressActivity extends BaseActivity {

    public static final int REQUEST_CODE_FIND_ADDRESS = 1;
    public static final String ARG_LAT_LNG = "latlng";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    public static Intent newInstance(Context context) {
        return new Intent(context, PickupAddressActivity.class);
    }

    @Override
    public void setUpComponent(ApplicationComponent appComponent) {
    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_pickup_address;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Navigator.callPickupAddressFragment(getSupportFragmentManager(), R.id.pickupAddressContent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pickup_address, menu);
        return true;
    }
}
