package com.direito.challenge99.pickupaddress;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.widget.EditText;
import butterknife.BindView;
import com.direito.challenge99.R;
import com.direito.challenge99.common.BaseFragment;
import com.direito.challenge99.common.BasePresenter;
import com.direito.challenge99.internal.di.components.ApplicationComponent;
import com.direito.challenge99.internal.di.components.DaggerPickupAddressComponent;
import com.direito.challenge99.internal.di.modules.PickupAddressModule;
import com.direito.challenge99.util.AlertUtils;
import com.direito.challenge99.util.ViewUtils;
import com.google.android.gms.maps.model.LatLng;

import javax.inject.Inject;

public class PickupAddressFragment extends BaseFragment implements PickupAddressContract.View {
    private static final String TAG = "PickupAddressFragment";

    @Inject
    PickupAddressPresenter pickupAddressPresenter;

    @BindView(R.id.streetEditText)
    EditText streetEditText;
    @BindView(R.id.numberEditText)
    EditText numberTextInputLayout;
    @BindView(R.id.cityEditText)
    EditText cityTextInputLayout;
    @BindView(R.id.districtEditText)
    EditText districtTextInputLayout;

    @Override
    protected void setUpComponent(ApplicationComponent component) {
        DaggerPickupAddressComponent.builder()
                .applicationComponent(component)
                .pickupAddressModule(new PickupAddressModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected BasePresenter getPresenter() {
        return pickupAddressPresenter;
    }

    @Override
    public String tag() {
        return TAG;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_pickup_address;
    }

    public PickupAddressFragment() {
    }

    public static PickupAddressFragment newInstance() {
        PickupAddressFragment fragment = new PickupAddressFragment();
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            case R.id.action_done:
                ViewUtils.hideSoftKeyboard(getActivity());
                pickupAddressPresenter.searchAddress(CONTEXT, getTypedAddress());
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showMessage(String message) {
        AlertUtils.showMessage(
                getActivity().getWindow().getDecorView().findViewById(android.R.id.content),
                message);
    }

    @Override
    public void updateLocation(LatLng latLng) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(PickupAddressActivity.ARG_LAT_LNG, latLng);

        getActivity().setResult(Activity.RESULT_OK, returnIntent);
        getActivity().finish();
    }

    @Override
    public void showLoading() {
        showProgressLoading();
    }

    @Override
    public void showError() {
        hideProgressLoading();
    }

    @Override
    public void showContent() {
        hideProgressLoading();
    }

    public String getTypedAddress() {
        return new StringBuilder(streetEditText.getText().toString()).append(" ")
                .append(numberTextInputLayout.getText().toString()).append(" ")
                .append(cityTextInputLayout.getText().toString()).append(" ")
                .append(districtTextInputLayout.getText().toString())
                .toString();
    }
}
