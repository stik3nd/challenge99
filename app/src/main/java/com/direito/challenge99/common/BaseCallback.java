package com.direito.challenge99.common;

public interface BaseCallback {

    void onLoadError(String errorResponse);

}
