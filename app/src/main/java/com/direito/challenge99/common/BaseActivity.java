package com.direito.challenge99.common;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import butterknife.ButterKnife;
import com.direito.challenge99.MainApplication;
import com.direito.challenge99.internal.di.components.ApplicationComponent;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        injectDependencies();
        injectViews();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (getPresenter() != null)
            getPresenter().onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (getPresenter() != null)
            getPresenter().onStop();
    }

    private void injectDependencies() {
        setUpComponent(MainApplication.getApp(this).getComponent());
    }

    public abstract void setUpComponent(ApplicationComponent appComponent);

    protected abstract BasePresenter getPresenter();

    private void injectViews() {
        ButterKnife.bind(this);
    }

    protected abstract int getLayout();
}
