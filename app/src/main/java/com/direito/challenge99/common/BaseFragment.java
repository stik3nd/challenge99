package com.direito.challenge99.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.direito.challenge99.MainApplication;
import com.direito.challenge99.internal.di.components.ApplicationComponent;

public abstract class BaseFragment extends Fragment {
    protected Context CONTEXT;
    private Unbinder unbinder;

    public ProgressDialog progressDialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        CONTEXT = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);
        return inflater.inflate(getFragmentLayout(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews(view);
        injectDependencies();
        progressDialog = new ProgressDialog(CONTEXT);
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        getPresenter().onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbindViews();
    }

    private void injectDependencies() {
        setUpComponent(MainApplication.getApp(getActivity()).getComponent());
    }

    protected abstract void setUpComponent(ApplicationComponent component);

    protected abstract BasePresenter getPresenter();

    private void bindViews(View rootView) {
        unbinder = ButterKnife.bind(this, rootView);
    }

    private void unbindViews() {
        unbinder.unbind();
    }

    public abstract String tag();

    protected abstract int getFragmentLayout();

    public void showProgressLoading() {
        if (progressDialog != null)
            progressDialog.show();
    }

    public void hideProgressLoading() {
        if (progressDialog != null)
            progressDialog.hide();
    }
}
