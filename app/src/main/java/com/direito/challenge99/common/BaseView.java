package com.direito.challenge99.common;

public interface BaseView {

    void showLoading();

    void showError();

    void showContent();

}
