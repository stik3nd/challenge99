package com.direito.challenge99.common;

public interface BasePresenter {

    void onStart();

    void onStop();

}