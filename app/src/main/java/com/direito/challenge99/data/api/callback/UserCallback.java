package com.direito.challenge99.data.api.callback;

import com.direito.challenge99.common.BaseCallback;
import com.direito.challenge99.data.api.model.User;

public interface UserCallback extends BaseCallback {

    void onUserLoaded(User user);

}
