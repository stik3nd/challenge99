package com.direito.challenge99.data.api.callback;

import com.direito.challenge99.common.BaseCallback;
import com.direito.challenge99.data.api.model.Ride;

public interface RideCallback extends BaseCallback {

    void onRideLoaded(Ride ride);

}
