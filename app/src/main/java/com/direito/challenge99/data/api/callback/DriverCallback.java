package com.direito.challenge99.data.api.callback;

import com.direito.challenge99.common.BaseCallback;
import com.direito.challenge99.data.api.model.Driver;

import java.util.List;

public interface DriverCallback extends BaseCallback {

    void onDriversLoaded(List<Driver> drivers);

}
