package com.direito.challenge99.data.api;

public class ApiConstants {
    public static final class URL {
        public static final String BASE = "http://ec2-54-88-12-34.compute-1.amazonaws.com/";
        public static final String BASE_99_HOST = "https://api.99taxis.com/";

        public static final String LAST_LOCATIONS_URL = BASE_99_HOST +
                Method.LAST_LOCATIONS;
        public static final String REQUEST_RIDE = Version.v1 +
                Method.RIDE;
        public static final String RIDE = Version.v1 +
                Method.RIDE + "/" +
                "{" + Path.RIDE_ID + "}";
        public static final String CREATE_USER = Version.v1 +
                Method.USER;
        public static final String UPDATE_USER = Version.v1 +
                Method.USER + "/" +
                "{" + Path.USER_ID + "}";
        public static final String USER = Version.v1 +
                Method.USER + "/" +
                "{" + Path.USER_ID + "}";
    }

    public static final class Version {
        public static final String v1 = "v1/";
    }

    public static final class Method {
        public static final String LAST_LOCATIONS = "lastLocations";
        public static final String RIDE = "ride";
        public static final String USER = "users";

    }

    public static final class Query {
        public static final String LAST_LOCATIONS_SOUTH_WEST = "sw";
        public static final String LAST_LOCATIONS_NORTH_EAST = "ne";
        public static final String NAME = "name";
    }

    public static final class Path {
        public static final String RIDE_ID = "rideId";
        public static final String USER_ID = "userId";
    }
}
