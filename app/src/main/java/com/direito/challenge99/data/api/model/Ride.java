package com.direito.challenge99.data.api.model;

import com.google.gson.annotations.SerializedName;

public class Ride {
    @SerializedName("rideId")
    private int rideId;
    @SerializedName("msg")
    private String message;

    public Ride(int rideId, String message) {
        this.rideId = rideId;
        this.message = message;
    }

    public int getRideId() {
        return rideId;
    }

    public String getMessage() {
        return message;
    }
}
