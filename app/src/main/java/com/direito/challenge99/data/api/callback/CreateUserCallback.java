package com.direito.challenge99.data.api.callback;

import com.direito.challenge99.common.BaseCallback;

public interface CreateUserCallback extends BaseCallback {

    void onUserCreated(String userUrl);

}
