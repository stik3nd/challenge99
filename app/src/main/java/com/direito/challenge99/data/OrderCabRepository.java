package com.direito.challenge99.data;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import com.direito.challenge99.R;
import com.direito.challenge99.data.api.ApiConstants;
import com.direito.challenge99.data.api.ApiService;
import com.direito.challenge99.data.api.callback.DriverCallback;
import com.direito.challenge99.data.api.callback.RideCallback;
import com.direito.challenge99.data.api.model.Driver;
import com.direito.challenge99.data.api.model.Ride;
import com.direito.challenge99.util.ErrorUtils;
import com.direito.challenge99.util.StringUtils;
import com.direito.challenge99.util.Transformers;
import com.google.android.gms.maps.model.LatLng;
import retrofit2.Response;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.functions.Func1;

import java.util.List;
import java.util.Locale;

public class OrderCabRepository {
    private static final String TAG = "OrderCabRepository";

    private ApiService apiService;

    public OrderCabRepository(ApiService apiService) {
        this.apiService = apiService;
    }

    public void getNearbyDrivers(final String southWestLatLng, final String northEastLatLng, final DriverCallback callback) {
        apiService.findNearbyDrivers(ApiConstants.URL.LAST_LOCATIONS_URL, southWestLatLng, northEastLatLng)
                .compose(Transformers.applySchedulers())
                .subscribe(new Observer<Response<List<Driver>>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onLoadError("Error: " + e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(Response<List<Driver>> response) {
                        if (response.isSuccessful()) {
                            List<Driver> drivers = response.body();
                            callback.onDriversLoaded(drivers);
                            return;
                        }

                        callback.onLoadError(
                                ErrorUtils.handleError(response, null));
                    }
                });
    }

    public Observable<String> getAddress(final Context context, final double lat, final double lng) {
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                String result = null;
                final Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                try {
                    result = geocoder.getFromLocation(lat, lng, 1).get(0).getAddressLine(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (result == null) {
                    result = context.getString(R.string.not_found);
                }

                subscriber.onNext(result);
                subscriber.onCompleted();
            }
        });
    }

    public Observable<LatLng> getLatLngFromAddress(final Context context, final String address) {
        return Observable.create(new Observable.OnSubscribe<LatLng>() {
            @Override
            public void call(Subscriber<? super LatLng> subscriber) {
                LatLng result = null;
                final Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                try {
                    Address foundAddress = geocoder.getFromLocationName(address, 1).get(0);
                    result = new LatLng(foundAddress.getLatitude(), foundAddress.getLongitude());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                subscriber.onNext(result);
                subscriber.onCompleted();
            }
        });
    }

    public Observable<String> requestRide() {
        return apiService.requestRide()
                .flatMap((Func1<Response<Void>, Observable<String>>) response -> {
                    if (response.isSuccessful()) {
                        String rideUrl = response.headers().get("Location");
                        if (!StringUtils.isNullOrEmpty(rideUrl)) {
                            return Observable.just(rideUrl);
                        }
                    }

                    return Observable.error(new Throwable(ErrorUtils.handleError(response, new int[]{410})));
                });
    }

    public void getRide(final String rideUrl, final RideCallback callback) {
        apiService.findRideWithUrl(rideUrl)
                .compose(Transformers.applySchedulers())
                .subscribe(new Observer<Response<Ride>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onLoadError("Error: " + e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(Response<Ride> response) {
                        if (response.isSuccessful()) {
                            callback.onRideLoaded(response.body());
                            return;
                        }

                        callback.onLoadError(
                                ErrorUtils.handleError(response, null));
                    }
                });
    }

}
