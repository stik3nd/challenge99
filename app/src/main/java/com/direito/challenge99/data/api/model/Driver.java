package com.direito.challenge99.data.api.model;

import com.google.gson.annotations.SerializedName;

public class Driver {
    @SerializedName("latitude")
    private double latitude;
    @SerializedName("longitude")
    private double longitude;
    @SerializedName("driverId")
    private int driverId;
    @SerializedName("driverAvailable")
    private boolean driverAvailable;

    public Driver(double latitude, double longitude, int driverId, boolean driverAvailable) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.driverId = driverId;
        this.driverAvailable = driverAvailable;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public int getDriverId() {
        return driverId;
    }

    public boolean isDriverAvailable() {
        return driverAvailable;
    }
}
