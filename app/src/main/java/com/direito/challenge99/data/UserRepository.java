package com.direito.challenge99.data;

import com.direito.challenge99.data.api.ApiService;
import com.direito.challenge99.data.api.callback.CreateUserCallback;
import com.direito.challenge99.data.api.callback.UserCallback;
import com.direito.challenge99.data.api.model.User;
import com.direito.challenge99.util.ErrorUtils;
import com.direito.challenge99.util.StringUtils;
import com.direito.challenge99.util.Transformers;
import retrofit2.Response;
import rx.Observer;

public class UserRepository {
    private static final String TAG = "UserRepository";

    private ApiService apiService;

    public UserRepository(ApiService apiService) {
        this.apiService = apiService;
    }

    public void createUser(final String name, final CreateUserCallback callback) {
        apiService.createUser(name)
                .compose(Transformers.applySchedulers())
                .subscribe(new Observer<Response<Void>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onLoadError("Error: " + e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(Response<Void> response) {
                        if (response.isSuccessful()) {
                            String userUrl = response.headers().get("Location");
                            if (!StringUtils.isNullOrEmpty(userUrl)) {
                                callback.onUserCreated(userUrl);
                                return;
                            }
                        }

                        callback.onLoadError(
                                ErrorUtils.handleError(response, new int[]{400}));
                    }
                });
    }

    public void getUser(final String userUrl, final UserCallback callback) {
        apiService.findUserWithUrl(userUrl)
                .compose(Transformers.applySchedulers())
                .subscribe(new Observer<Response<User>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onLoadError("Error: " + e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(Response<User> response) {
                        if (response.isSuccessful()) {
                            callback.onUserLoaded(response.body());
                            return;
                        }

                        callback.onLoadError(
                                ErrorUtils.handleError(response, new int[]{404}));
                    }
                });
    }

    public void updateUser(final int userId, final String name, final UserCallback callback) {
        apiService.updateUser(userId, name)
                .compose(Transformers.applySchedulers())
                .subscribe(new Observer<Response<User>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onLoadError("Error: " + e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(Response<User> response) {
                        if (response.isSuccessful()) {
                            callback.onUserLoaded(response.body());
                            return;
                        }

                        callback.onLoadError(
                                ErrorUtils.handleError(response, new int[]{400, 404}));
                    }
                });
    }

}
