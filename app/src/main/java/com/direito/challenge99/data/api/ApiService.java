package com.direito.challenge99.data.api;

import com.direito.challenge99.data.api.model.Driver;
import com.direito.challenge99.data.api.model.Ride;
import com.direito.challenge99.data.api.model.User;
import retrofit2.Response;
import retrofit2.http.*;
import rx.Observable;

import java.util.List;

public interface ApiService {

    @GET
    Observable<Response<List<Driver>>> findNearbyDrivers(
            @Url String url,
            @Query(ApiConstants.Query.LAST_LOCATIONS_SOUTH_WEST) String southWestLatLng,
            @Query(ApiConstants.Query.LAST_LOCATIONS_NORTH_EAST) String northEastLatLng);

    @PUT(ApiConstants.URL.REQUEST_RIDE)
    Observable<Response<Void>> requestRide();

    @GET(ApiConstants.URL.RIDE)
    Observable<Response<Ride>> findRideById(
            @Path(ApiConstants.Path.RIDE_ID) int rideId);

    @GET
    Observable<Response<Ride>> findRideWithUrl(
            @Url String rideUrl);

    @FormUrlEncoded
    @POST(ApiConstants.URL.CREATE_USER)
    Observable<Response<Void>> createUser(
            @Field(ApiConstants.Query.NAME) String name);

    @FormUrlEncoded
    @POST(ApiConstants.URL.UPDATE_USER)
    Observable<Response<User>> updateUser(
            @Path(ApiConstants.Path.USER_ID) int userId,
            @Field(ApiConstants.Query.NAME) String name);
//            @Query(ApiConstants.Query.NAME) String name)

    @GET(ApiConstants.URL.USER)
    Observable<Response<User>> findUserById(
            @Path(ApiConstants.Path.USER_ID) int userId);

    @GET
    Observable<Response<User>> findUserWithUrl(
            @Url String userUrl);

}
