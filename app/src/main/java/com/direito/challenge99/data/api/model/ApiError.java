package com.direito.challenge99.data.api.model;

import com.direito.challenge99.util.StringUtils;
import com.google.gson.annotations.SerializedName;

public class ApiError {
    @SerializedName("msg")
    private String message;
    @SerializedName("message")
    private String anotherMessage;

    public String getMessage() {
        return String.valueOf(StringUtils.isNullOrEmpty(anotherMessage) ? "" : anotherMessage) +
                String.valueOf(StringUtils.isNullOrEmpty(message) ? "" : message);
    }
}