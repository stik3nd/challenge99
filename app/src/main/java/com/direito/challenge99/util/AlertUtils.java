package com.direito.challenge99.util;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.widget.TextView;
import com.direito.challenge99.R;

public final class AlertUtils {
    public static final int LENGTH_LONGER = 5000;
    private static final boolean DEFAULT_DISMISS_ACTION = true;

    private AlertUtils() {
    }

    public static void showMessage(final View view, final int message) {
        showMessage(view, view.getResources().getString(message), false, DEFAULT_DISMISS_ACTION);
    }

    public static void showMessage(final View view, final String message) {
        showMessage(view, message, false, DEFAULT_DISMISS_ACTION);
    }

    public static void showMessage(final View view, final int message, boolean indefinite, boolean dismissAction) {
        showMessage(view, view.getResources().getString(message), indefinite, dismissAction);
    }

    public static void showMessage(final View view, final String message, boolean indefinite, boolean dismissAction) {
        int duration = LENGTH_LONGER;
        if (indefinite)
            duration = Snackbar.LENGTH_INDEFINITE;
        final Snackbar snackbar = Snackbar.make(view, message, duration)
                .setActionTextColor(Color.WHITE);

        if (dismissAction) {
            snackbar.setAction(R.string.ok, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    snackbar.dismiss();
                }
            });
        }

        snackbar.setCallback(new Snackbar.Callback()

                             {
                                 @Override
                                 public void onShown(Snackbar snackbar) {
                                     snackbar.getView().setContentDescription(message);
                                     snackbar.getView().sendAccessibilityEvent(AccessibilityEvent.TYPE_ANNOUNCEMENT);
                                 }
                             }

        );
        View snackBarView = snackbar.getView();
        //        snackBarView.setBackgroundColor(Color.parseColor("#009688"));
        TextView tvSnack = (TextView)
                snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        tvSnack.setMaxLines(3);

        snackbar.show();
    }

    public static void showMessageDialog(final Context context, final String message) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", null)
                .show();
    }
}
