package com.direito.challenge99.util;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

public class LocationUtils {

    public static final float DEFAULT_ZOOM_LEVEL = 16.0f;

    public static void moveCamera(GoogleMap map, LatLng position) {
        CameraPosition cameraPosition = new CameraPosition.Builder().target(position).zoom(DEFAULT_ZOOM_LEVEL).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public static LatLng getMyLastPosition(Context context) {
        try {
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            Location deviceLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            return new LatLng(deviceLocation.getLatitude(), deviceLocation.getLongitude());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String getFormattedLatLng(LatLng latLng) {
        if (latLng == null)
            return null;

         return latLng.latitude +
                "," +
                latLng.longitude;
    }
}
