package com.direito.challenge99.util;

public class StringUtils {

    public static boolean isNullOrEmpty(String string) {
        return string == null || string.length() == 0; // string.isEmpty() in Java 6
    }
}
