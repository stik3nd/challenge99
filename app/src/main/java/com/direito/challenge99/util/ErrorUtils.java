package com.direito.challenge99.util;

import com.direito.challenge99.data.api.ApiManager;
import com.direito.challenge99.data.api.model.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

import java.io.IOException;
import java.lang.annotation.Annotation;

public class ErrorUtils {

    public static ApiError parseError(Response<?> response) {
        Converter<ResponseBody, ApiError> converter =
                ApiManager.getInstance()
                        .responseBodyConverter(ApiError.class, new Annotation[0]);

        ApiError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ApiError();
        }

        return error;
    }

    public static String handleError(Response<?> response, int[] knownErrorCodes) {
        if (knownErrorCodes != null) {
            boolean isKnownError = false;

            for (int errorCode : knownErrorCodes) {
                if (response.code() == errorCode) {
                    isKnownError = true;
                    break;
                }
            }

            if (isKnownError) {
                ApiError error = ErrorUtils.parseError(response);
                return error.getMessage();
            }
        }

        ResponseBody errorBody = response.errorBody();
        try {
            return String.valueOf(response.code()) +
                    " - " +
                    errorBody.string();
        } catch (Exception e) {
            e.printStackTrace();
            return "Error: " + e.getLocalizedMessage();
        }
    }
}
