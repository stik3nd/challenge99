package com.direito.challenge99.util;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import com.direito.challenge99.ordercab.OrderCabFragment;
import com.direito.challenge99.pickupaddress.PickupAddressActivity;
import com.direito.challenge99.pickupaddress.PickupAddressFragment;
import com.direito.challenge99.user.UserActivity;
import com.direito.challenge99.user.UserFragment;

public class Navigator {

    public static void callOrderCabFragment(final FragmentManager fragmentManager, @IdRes final int layoutId) {
        OrderCabFragment orderCabFragment = OrderCabFragment.newInstance();
        FragmentUtils.replaceFragment(fragmentManager, orderCabFragment, layoutId, false);
    }

    public static void callUserFragment(final FragmentManager fragmentManager, @IdRes final int layoutId) {
        UserFragment userFragment = UserFragment.newInstance();
        FragmentUtils.replaceFragment(fragmentManager, userFragment, layoutId, false);
    }

    public static void callPickupAddressFragment(final FragmentManager fragmentManager, @IdRes final int layoutId) {
        PickupAddressFragment pickupAddressFragment = PickupAddressFragment.newInstance();
        FragmentUtils.replaceFragment(fragmentManager, pickupAddressFragment, layoutId, false);
    }

    public static void callUserActivity(Context context) {
        context.startActivity(UserActivity.newInstance(context));
    }

    public static void callPickupAddressActivity(Fragment fragment) {
        Intent intent = PickupAddressActivity.newInstance(fragment.getContext());
        fragment.startActivityForResult(intent, PickupAddressActivity.REQUEST_CODE_FIND_ADDRESS);
    }
}
