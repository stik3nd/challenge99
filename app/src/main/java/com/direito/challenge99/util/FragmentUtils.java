package com.direito.challenge99.util;

import android.support.annotation.AnimRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import com.direito.challenge99.common.BaseFragment;

public class FragmentUtils {

    public static FragmentTransaction ensureTransaction(final FragmentManager fragmentManager) {
        return fragmentManager.beginTransaction();
    }

    public static android.app.FragmentTransaction ensureTransaction(final android.app.FragmentManager fragmentManager) {
        return fragmentManager.beginTransaction();
    }

    public static Fragment getFragment(final FragmentManager fragmentManager, final String tag) {
        return fragmentManager.findFragmentByTag(tag);
    }

    public static Fragment getFragment(final FragmentManager fragmentManager, final int id) {
        return fragmentManager.findFragmentById(id);
    }

    public static void attachFragment(final FragmentTransaction fragmentTransaction, final int content, final BaseFragment fragment) {
        if (fragment != null) {
            if (fragment.isDetached()) {
                fragmentTransaction.attach(fragment);
            } else if (!fragment.isAdded()) {
                fragmentTransaction.add(content, fragment, fragment.tag());
            }
            fragmentTransaction.commit();
        }
    }

    public static void attachFragmentWithAnimation(final FragmentTransaction fragmentTransaction, final int containerId, final BaseFragment fragment, boolean addToBackStack) {
//        fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);

        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragment.tag());
        }
        FragmentUtils.attachFragment(fragmentTransaction, containerId, fragment);
    }

    public static void reAttachFragment(final FragmentTransaction fragmentTransaction, final Fragment fragment) {
        fragmentTransaction.attach(fragment);
    }

    public static void detachFragment(final FragmentTransaction fragmentTransaction, final Fragment fragment) {
        if (fragment != null && !fragment.isDetached()) {
            fragmentTransaction.detach(fragment);
        }
    }

    public static void removeFragment(final FragmentManager fragmentManager, final Fragment fragment, final boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = ensureTransaction(fragmentManager);
        fragmentTransaction.remove(fragment);
        commitTransactions(fragmentTransaction, addToBackStack);
    }

    public static void replaceFragmentWithAnimation(final FragmentTransaction fragmentTransaction, final BaseFragment fragment, final int containerToReplace, final boolean addToBackStack, @AnimRes final int animationEnter, @AnimRes final int animationExit) {
        if (addToBackStack)
            fragmentTransaction.addToBackStack(fragment.tag());
        fragmentTransaction.setCustomAnimations(animationEnter, animationExit);
        fragmentTransaction.replace(containerToReplace, fragment).commit();
    }

    public static void replaceFragmentWithManyAnimations(final FragmentTransaction fragmentTransaction, final BaseFragment fragment, final int containerToReplace, final boolean addToBackStack, @AnimRes final int animationEnter, @AnimRes final int animationExit, @AnimRes final int animationPopEnter, @AnimRes final int animationPopExit) {
        if (addToBackStack)
            fragmentTransaction.addToBackStack(fragment.tag());
        fragmentTransaction.setCustomAnimations(animationEnter, animationExit, animationPopEnter, animationPopExit);
        fragmentTransaction.replace(containerToReplace, fragment, fragment.tag()).commit();
    }

    public static void replaceFragment(final FragmentManager fragmentManager, final BaseFragment fragment, final int containerToReplace, final boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = ensureTransaction(fragmentManager);
        fragmentTransaction.replace(containerToReplace, fragment, fragment.tag());
        commitTransactions(fragmentTransaction, addToBackStack);
    }

    public static void replaceFragment(final FragmentManager fragmentManager, final Fragment fragment, final int containerToReplace, final boolean addToBackStack, String tag) {
        FragmentTransaction fragmentTransaction = ensureTransaction(fragmentManager);
        fragmentTransaction.replace(containerToReplace, fragment, tag);
        commitTransactions(fragmentTransaction, addToBackStack);
    }

    public static void commitTransactions(final FragmentTransaction fragmentTransaction) {
        commitTransactions(fragmentTransaction, false);
    }

    public static void commitTransactions(final FragmentTransaction fragmentTransaction, final boolean addToBackStack) {
        if (fragmentTransaction != null && !fragmentTransaction.isEmpty()) {
            if (addToBackStack)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends Fragment> T findFragment(final FragmentManager fragmentManager, final String tag) {
        return (T) fragmentManager.findFragmentByTag(tag);
    }

    @SuppressWarnings("unchecked")
    public static <T extends Fragment> T findFragment(final FragmentManager fragmentManager, final int id) {
        return (T) fragmentManager.findFragmentById(id);
    }

    public static void resetBackStack(final FragmentManager fragmentManager) {
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public static void backToFirstFragment(final FragmentManager fragmentManager) {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = fragmentManager.getBackStackEntryAt(0);
            fragmentManager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public static Fragment getCurrentFragment(final FragmentManager fragmentManager) {
        String fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
        Fragment currentFragment = fragmentManager
                .findFragmentByTag(fragmentTag);
        return currentFragment;
    }

}
