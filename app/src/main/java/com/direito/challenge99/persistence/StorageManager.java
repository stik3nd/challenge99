package com.direito.challenge99.persistence;

import java.util.List;

public interface StorageManager {
    public <T> T get(String key, Class<T> clazz);

    public <T> void put(String key, T value);

    public boolean contain(String key);

    public void delete(String key);

    public <T> List<T> getList(final String key, Class<T[]> clazz);
}
