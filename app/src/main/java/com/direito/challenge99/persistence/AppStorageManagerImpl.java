package com.direito.challenge99.persistence;

import android.content.Context;
import com.direito.challenge99.data.api.model.User;

public class AppStorageManagerImpl implements AppStorageManager {

    public static final String APP_STORAGE_NAME = "CHALLENGE99_STORAGE";
    private static final String USER_KEY = "USER_KEY";

    private Context context;
    private StorageManager storageManager;

    public AppStorageManagerImpl(Context context) {
        this.context = context;
        storageManager = new StorageManagerImpl(this.context, APP_STORAGE_NAME);
    }

    @Override
    public void saveUser(User user) {
        storageManager.put(USER_KEY, user);
    }

    @Override
    public User getUser() {
        return storageManager.get(USER_KEY, User.class);
    }

    @Override
    public boolean containsUser() {
        return storageManager.contain(USER_KEY);
    }

}
