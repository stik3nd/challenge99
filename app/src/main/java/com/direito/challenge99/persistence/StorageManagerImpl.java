package com.direito.challenge99.persistence;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class StorageManagerImpl implements StorageManager {

    private SharedPreferences sharedPreferences;
    private Gson gson;

    public StorageManagerImpl(final Context context, final String storageName) {
        sharedPreferences = context.getSharedPreferences(storageName, Context.MODE_PRIVATE);
        gson = new Gson();
    }

    @Override
    public <T> T get(String key, Class<T> clazz) {
        return gson.fromJson(sharedPreferences.getString(key, null), clazz);
    }

    @Override
    public <T> void put(String key, T value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, gson.toJson(value));
        editor.apply();
    }

    @Override
    public boolean contain(String key) {
        return sharedPreferences.contains(key);
    }

    @Override
    public void delete(String key) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.apply();
    }

    @Override
    public <T> List<T> getList(String key, Class<T[]> clazz) {
        T[] array = gson.fromJson(sharedPreferences.getString(key, null), clazz);
        return new LinkedList<>(Arrays.asList(array));
    }


}
