package com.direito.challenge99.persistence;


import com.direito.challenge99.data.api.model.User;

public interface AppStorageManager {

    void saveUser(final User user);
    User getUser();
    boolean containsUser();
}