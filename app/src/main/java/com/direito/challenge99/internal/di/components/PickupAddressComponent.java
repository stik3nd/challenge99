package com.direito.challenge99.internal.di.components;

import com.direito.challenge99.internal.di.ActivityScope;
import com.direito.challenge99.internal.di.modules.PickupAddressModule;
import com.direito.challenge99.pickupaddress.PickupAddressFragment;
import com.direito.challenge99.pickupaddress.PickupAddressPresenter;
import dagger.Component;

@ActivityScope
@Component(
        dependencies = ApplicationComponent.class,
        modules = PickupAddressModule.class
)
public interface PickupAddressComponent {

    void inject(PickupAddressFragment pickupAddressFragment);
    PickupAddressPresenter getPresenter();

}
