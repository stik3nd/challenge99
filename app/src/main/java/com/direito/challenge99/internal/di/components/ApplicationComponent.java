package com.direito.challenge99.internal.di.components;

import android.content.Context;
import com.direito.challenge99.data.OrderCabRepository;
import com.direito.challenge99.data.UserRepository;
import com.direito.challenge99.internal.di.modules.ApplicationModule;
import com.direito.challenge99.internal.di.modules.RepositoryModule;
import com.direito.challenge99.persistence.AppStorageManagerImpl;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                RepositoryModule.class
        }
)
public interface ApplicationComponent {

    Context getContext();
    OrderCabRepository getOrderCabRepository();
    UserRepository getUserRepository();
    AppStorageManagerImpl getSharedPreferences();

}