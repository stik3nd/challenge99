package com.direito.challenge99.internal.di.modules;

import com.direito.challenge99.data.UserRepository;
import com.direito.challenge99.user.UserContract;
import com.direito.challenge99.user.UserPresenter;
import dagger.Module;
import dagger.Provides;

@Module
public class UserModule {
    private final UserContract.View view;

    public UserModule(UserContract.View view) {
        this.view = view;
    }

    @Provides
    public UserContract.View provideView() {
        return view;
    }

    @Provides
    public UserPresenter providePresenter(UserContract.View view, UserRepository repository) {
        return new UserPresenter(view, repository);
    }
}
