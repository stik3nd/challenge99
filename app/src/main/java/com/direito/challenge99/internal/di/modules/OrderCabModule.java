package com.direito.challenge99.internal.di.modules;

import com.direito.challenge99.data.OrderCabRepository;
import com.direito.challenge99.ordercab.OrderCabContract;
import com.direito.challenge99.ordercab.OrderCabPresenter;
import dagger.Module;
import dagger.Provides;

@Module
public class OrderCabModule {
    private final OrderCabContract.View view;

    public OrderCabModule(OrderCabContract.View view) {
        this.view = view;
    }

    @Provides
    public OrderCabContract.View provideView() {
        return view;
    }

    @Provides
    public OrderCabPresenter providePresenter(OrderCabContract.View view, OrderCabRepository repository) {
        return new OrderCabPresenter(view, repository);
    }
}
