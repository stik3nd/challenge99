package com.direito.challenge99.internal.di.components;

import com.direito.challenge99.internal.di.ActivityScope;
import com.direito.challenge99.internal.di.modules.OrderCabModule;
import com.direito.challenge99.ordercab.OrderCabFragment;
import com.direito.challenge99.ordercab.OrderCabPresenter;
import dagger.Component;

@ActivityScope
@Component(
        dependencies = ApplicationComponent.class,
        modules = OrderCabModule.class
)
public interface OrderCabComponent {

    void inject(OrderCabFragment orderCabFragment);
    OrderCabPresenter getPresenter();

}
