package com.direito.challenge99.internal.di.modules;

import com.direito.challenge99.data.OrderCabRepository;
import com.direito.challenge99.data.UserRepository;
import com.direito.challenge99.data.api.ApiService;
import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @Provides
    public OrderCabRepository provideOrderCabRepository(ApiService apiService){
        return new OrderCabRepository(apiService);
    }

    @Provides
    public UserRepository provideUserRepository(ApiService apiService){
        return new UserRepository(apiService);
    }

}
