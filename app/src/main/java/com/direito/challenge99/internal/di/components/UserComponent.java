package com.direito.challenge99.internal.di.components;

import com.direito.challenge99.internal.di.ActivityScope;
import com.direito.challenge99.internal.di.modules.UserModule;
import com.direito.challenge99.user.UserFragment;
import com.direito.challenge99.user.UserPresenter;
import dagger.Component;

@ActivityScope
@Component(
        dependencies = ApplicationComponent.class,
        modules = UserModule.class
)
public interface UserComponent {

    void inject(UserFragment userFragment);
    UserPresenter getPresenter();

}
