package com.direito.challenge99.internal.di.modules;

import com.direito.challenge99.data.OrderCabRepository;
import com.direito.challenge99.pickupaddress.PickupAddressContract;
import com.direito.challenge99.pickupaddress.PickupAddressPresenter;
import dagger.Module;
import dagger.Provides;

@Module
public class PickupAddressModule {
    private final PickupAddressContract.View view;

    public PickupAddressModule(PickupAddressContract.View view) {
        this.view = view;
    }

    @Provides
    public PickupAddressContract.View provideView() {
        return view;
    }

    @Provides
    public PickupAddressPresenter providePresenter(PickupAddressContract.View view, OrderCabRepository repository) {
        return new PickupAddressPresenter(view, repository);
    }
}
