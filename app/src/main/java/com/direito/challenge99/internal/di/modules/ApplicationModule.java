package com.direito.challenge99.internal.di.modules;

import android.app.Application;
import android.content.Context;
import com.direito.challenge99.MainApplication;
import com.direito.challenge99.data.api.ApiManager;
import com.direito.challenge99.data.api.ApiService;
import com.direito.challenge99.persistence.AppStorageManagerImpl;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

import javax.inject.Singleton;

@Module
public class ApplicationModule {
    private MainApplication app;

    public ApplicationModule(MainApplication app) {
        this.app = app;
    }

    @Provides
    @Singleton
    public Application provideApplication() {
        return app;
    }

    @Provides
    @Singleton public Context provideContext() {
        return app;
    }

    @Provides
    @Singleton public Retrofit provideRetrofitInstance() {
        return ApiManager.getInstance();
    }

    @Provides
    @Singleton public ApiService provideApiService(Retrofit retrofit){
        return retrofit.create(ApiService.class);
    }

    @Provides
    @Singleton public AppStorageManagerImpl provideSharedPreferences(Context context) {
        return new AppStorageManagerImpl(context);
    }

}
