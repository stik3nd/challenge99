package com.direito.challenge99;

import android.app.Application;
import android.content.Context;
import com.direito.challenge99.internal.di.components.ApplicationComponent;
import com.direito.challenge99.internal.di.components.DaggerApplicationComponent;
import com.direito.challenge99.internal.di.modules.ApplicationModule;

public class MainApplication extends Application {

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        setupGraph();
    }

    private void setupGraph() {
        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
//                .repositoryModule()
                .build();
    }

    public ApplicationComponent getComponent() {
        return component;
    }

    public static MainApplication getApp(Context context) {
        return (MainApplication) context.getApplicationContext();
    }
}
