package com.direito.challenge99.user;

import com.direito.challenge99.data.api.callback.CreateUserCallback;
import com.direito.challenge99.data.api.callback.UserCallback;
import com.direito.challenge99.data.UserRepository;
import com.direito.challenge99.data.api.model.User;

public class UserPresenter implements UserContract.Presenter {

    private final UserContract.View userView;
    private final UserRepository userRepository;

    public UserPresenter(UserContract.View userView, UserRepository userRepository) {
        this.userView = userView;
        this.userRepository = userRepository;
    }

    @Override
    public void createUser(String name) {
        userView.showLoading();
        userRepository.createUser(name, new CreateUserCallback() {
            @Override
            public void onUserCreated(String userUrl) {
                getUser(userUrl);
            }

            @Override
            public void onLoadError(String errorResponse) {
                userView.showError();
                userView.showMessage(errorResponse);
            }
        });
    }

    @Override
    public void updateUser(int userId, String name) {
        userView.showLoading();
        userRepository.updateUser(userId, name, new UserCallback() {
            @Override
            public void onUserLoaded(User user) {
                userView.showContent();
                userView.saveUser(user);
                userView.showUserUpdatedMessage();
                userView.goBackToHome();
            }

            @Override
            public void onLoadError(String errorResponse) {
                userView.showError();
                userView.showMessage(errorResponse);
            }
        });
    }

    @Override
    public void getUser(String userUrl) {
        userRepository.getUser(userUrl, new UserCallback() {
            @Override
            public void onUserLoaded(User user) {
                userView.showContent();
                userView.saveUser(user);
                userView.showUserRegisteredMessage();
                userView.goBackToHome();
            }

            @Override
            public void onLoadError(String errorResponse) {
                userView.showError();
                userView.showMessage(errorResponse);
            }
        });
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }
}
