package com.direito.challenge99.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import butterknife.BindView;
import com.direito.challenge99.R;
import com.direito.challenge99.common.BaseActivity;
import com.direito.challenge99.common.BasePresenter;
import com.direito.challenge99.internal.di.components.ApplicationComponent;
import com.direito.challenge99.util.Navigator;

public class UserActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    public static Intent newInstance(Context context) {
        return new Intent(context, UserActivity.class);
    }

    @Override
    public void setUpComponent(ApplicationComponent appComponent) {
    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_user;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Navigator.callUserFragment(getSupportFragmentManager(), R.id.userContent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_user, menu);
        return true;
    }

}