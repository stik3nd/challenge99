package com.direito.challenge99.user;

import com.direito.challenge99.common.BasePresenter;
import com.direito.challenge99.common.BaseView;
import com.direito.challenge99.data.api.model.User;

public interface UserContract {

    interface View extends BaseView {

        void showMessage(String message);

        void showUserRegisteredMessage();

        void showUserUpdatedMessage();

        void saveUser(User user);

        void goBackToHome();

    }

    interface Presenter extends BasePresenter {

        void createUser(String name);

        void updateUser(int userId, String name);

        void getUser(String userUrl);

    }
}
