package com.direito.challenge99.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.BindView;
import com.direito.challenge99.R;
import com.direito.challenge99.common.BaseActivity;
import com.direito.challenge99.common.BaseFragment;
import com.direito.challenge99.common.BasePresenter;
import com.direito.challenge99.data.api.model.User;
import com.direito.challenge99.internal.di.components.ApplicationComponent;
import com.direito.challenge99.internal.di.components.DaggerUserComponent;
import com.direito.challenge99.internal.di.modules.UserModule;
import com.direito.challenge99.persistence.AppStorageManagerImpl;
import com.direito.challenge99.util.AlertUtils;
import com.direito.challenge99.util.StringUtils;
import com.direito.challenge99.util.ViewUtils;

import javax.inject.Inject;

public class UserFragment extends BaseFragment implements UserContract.View {
    private static final String TAG = "RegisterFragment";

    private AppStorageManagerImpl sharedPreferences;

    @Inject
    UserPresenter userPresenter;
    @BindView(R.id.nameEditText)
    EditText nameEditText;

    @Override
    protected void setUpComponent(ApplicationComponent component) {
        sharedPreferences = component.getSharedPreferences();

        DaggerUserComponent.builder()
                .applicationComponent(component)
                .userModule(new UserModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected BasePresenter getPresenter() {
        return userPresenter;
    }

    @Override
    public String tag() {
        return TAG;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_user;
    }

    public UserFragment() {
    }

    public static UserFragment newInstance() {
        UserFragment fragment = new UserFragment();
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (sharedPreferences.containsUser()) {
            ((BaseActivity) getActivity()).setTitle(getString(R.string.profile));
            nameEditText.setText(sharedPreferences.getUser().getName());
        } else {
            ((BaseActivity) getActivity()).setTitle(getString(R.string.register));
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            case R.id.action_done:
                clearFields();
                if (isValid())
                    insertOrUpdate();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void clearFields() {
        nameEditText.setError(null);
        ViewUtils.hideSoftKeyboard(getActivity());
    }

    private void insertOrUpdate() {
        if (sharedPreferences.containsUser()) {
            update();
        } else {
            create();
        }
    }

    private void update() {
        if (nameEditText.getText().toString().equals(sharedPreferences.getUser().getName())) {
            showMessage(getString(R.string.nothing_to_update));
            return;
        }

        userPresenter.updateUser(
                sharedPreferences.getUser().getUserId(),
                nameEditText.getText().toString());
    }

    private void create() {
        userPresenter.createUser(nameEditText.getText().toString());
    }

    private boolean isValid() {
        if (StringUtils.isNullOrEmpty(nameEditText.getText().toString())) {
            nameEditText.setError(getString(R.string.empty_field));
            return false;
        }

        return true;
    }

    @Override
    public void showMessage(String message) {
        AlertUtils.showMessage(
                getActivity().getWindow().getDecorView().findViewById(android.R.id.content),
                message);
    }

    @Override
    public void showUserRegisteredMessage() {
        Toast.makeText(getActivity(), getString(R.string.user_registered_successfully), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showUserUpdatedMessage() {
        Toast.makeText(getActivity(), getString(R.string.user_updated_successfully), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void saveUser(User user) {
        sharedPreferences.saveUser(user);
    }

    @Override
    public void goBackToHome() {
        getActivity().finish();
    }

    @Override
    public void showLoading() {
        showProgressLoading();
    }

    @Override
    public void showError() {
        hideProgressLoading();
    }

    @Override
    public void showContent() {
        hideProgressLoading();
    }
}
